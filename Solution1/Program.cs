﻿using System;

namespace Solution1
{
    public class Star
    {
        private double _starValue { get; set; }

        public string Name { get; set; }

        public Star()
        {
        }

        public Star(double value)
        {
            _starValue = value;
        }

        public double Shine()
        {
            if (_starValue < 0)
            {
                throw new InvalidOperationException("error");
            }
            return _starValue;
        }

        public void FadeOut()
        {
            _starValue = -1;
        }

        static void Main(string[] args)
        {
            // Code for debugging the test case NewStarsCanShine
            // double shineFactor = 1d;
            // Star star = new Star(shineFactor);
            // Console.WriteLine($"Are equal: {object.Equals(shineFactor, star.Shine())}, expected: {shineFactor}, actual: {star.Shine()}");
        }
    }
}
