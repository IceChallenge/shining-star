Visual Studio 2019 .NET Core 3.1 Console Application in C#

# Projects

    ShiningStarCodeChallenge - Code Challenge for Developer

    ShiningStarNUnitUnitTests - Test cases you can run against the Code Challenge
                              - Test cases you can run against Solution1 to see how each solution works

    Solution1 - One solution that solves each Test Case

# Instructions

    Open the "ShiningStarCodeChallenge" Project    
    Set your timer at 60:00 minutes and then start this exercise with the details below
    Run Unit Tests for "TestCasesForChallenge" to test your results

# Code Challenge

    A star simulation requires implementing a new Star class.  The NUnit unit tests that describe how the new class should behave have been provided here and below.

    using NUnit.Framework;
    using System;

    [TestFixture]
    public class Test
    {
        [Test]
        public void NewStarsCanShine()
        {
            double shineFactor = 1d;
            Star star = new Star(shineFactor);
            Assert.AreEqual(shineFactor, star.Shine());
        }

        [Test]
        public void FadedOutStarsCannotShine()
        {
            double shineFactor = 1.5d;
            Star star = new Star(shineFactor);
            star.FadeOut();
            Assert.Throws(typeof(InvalidOperationException), () => star.Shine());
        }

        [Test]
        public void StarsCanBeNamed()
        {
            double shineFactor = 0.8d;
            Star star = new Star(shineFactor);
            star.Name = "Sun";
            Assert.AreEqual("Sun", star.Name);
        }
    }