using NUnit.Framework;
using System;

namespace ShiningStarNUnitUnitTests
{
    internal abstract class BaseTests
    {
        #region Activator Support
        public string FullyQualifiedClassName{ get; set; }

        private object NewClassInstance(string fullyQualifiedName)
        {
            Type type = Type.GetType(fullyQualifiedName);

            if (type != null)
                return Activator.CreateInstance(type);

            return null;
        }

        private object NewClassInstance(string fullyQualifiedName, double shiningValue)
        {

            Type type = Type.GetType(fullyQualifiedName);

            if (type != null)
                return Activator.CreateInstance(type, args: new object[] { shiningValue });

            return null;
        }
        #endregion

        [Test]
        public void IntegrationTest()
        {
            string errorMessages = "";

            try
            {
                // Test for "Constructor" with parameter
                double shineFactor = 0.8d;
                dynamic star = NewClassInstance(FullyQualifiedClassName, shineFactor);
            }
            catch (Exception e)
            {
                errorMessages += e.Message + Environment.NewLine;
            }

            // Test for "Shine" Method
            try
            {
                dynamic star = NewClassInstance(FullyQualifiedClassName);
                star.Shine();
            }
            catch (Exception e)
            {
                errorMessages += e.Message + Environment.NewLine;
            }

            // Test for "Name" property
            try
            {
                dynamic star = NewClassInstance(FullyQualifiedClassName);
                star.Name = "Sun";
            }
            catch (Exception e)
            {
                errorMessages += e.Message + Environment.NewLine;
            }

            // Test for "FadeOut" Method
            try
            {
                dynamic star = NewClassInstance(FullyQualifiedClassName);
                star.FadeOut();
            }
            catch (Exception e)
            {
                errorMessages += e.Message + Environment.NewLine;
            }

            if (errorMessages != "")
            {
                throw new Exception(errorMessages);
            }

            Assert.Pass();
        }

        [Test]
        public void NewStarsCanShine()
        {
            double shineFactor = 1d;
            dynamic star = NewClassInstance(FullyQualifiedClassName, shineFactor);

            if (star == null)
            {
                // Framework update changed logic?  Developer changed Project/Class/Assembly info?
                // Unable to create instance of Class
                Assert.Fail();
            }

            Assert.AreEqual(shineFactor, star.Shine());
        }

        [Test]
        public void FadedOutStarsCannotShine()
        {
            double shineFactor = 1.5d;
            dynamic star = NewClassInstance(FullyQualifiedClassName, shineFactor);

            if (star == null)
            {
                // Framework update changed logic?  Developer changed Project/Class/Assembly info?
                // Unable to create instance of Class
                Assert.Fail();
            }

            star.FadeOut();
            Assert.Throws(typeof(InvalidOperationException), () => star.Shine());
        }

        [Test]
        public void StarsCanBeNamed()
        {
            double shineFactor = 0.8d;
            dynamic star = NewClassInstance(FullyQualifiedClassName, shineFactor);

            if (star == null)
            {
                // Framework update changed logic?  Developer changed Project/Class/Assembly info?
                // Unable to create instance of Class
                Assert.Fail();
            }

            star.Name = "Sun";
            Assert.AreEqual("Sun", star.Name);
        }

    }

}